export const getJobList = async () => {
	try {
		const requestOptions = {
			method: 'GET',
			headers: { 'Accept': 'application/json' }
		};
		const response = await fetch(`http://api.dataatwork.org/v1/jobs`, requestOptions)

		if (!response.ok) {
			throw new Error('Something went wrong in job list!');
		}

		const resData = await response.json();
		return resData;
	} catch (error) {
		console.error('---error in job list api---', error);
	}
};

export const getJobDetailsByUID = async (uid) => {
	try {
		const requestOptions = {
			method: 'GET',
			headers: { 'Accept': 'application/json' }
		};
		const response = await fetch(`http://api.dataatwork.org/v1/jobs/${uid}`, requestOptions)

		if (!response.ok) {
			throw new Error('Something went wrong in job details!');
		}

		const resData = await response.json();
		return resData;
	} catch (error) {
		console.error('---error in job job details api---', error);
	}
};