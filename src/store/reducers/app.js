import { act } from "react-dom/test-utils";

const initialState = {
	jobList: [],
}

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case 'SAVE_JOBS':
			return {
				...state,
				jobList: action.payload,
			}
		default:
			return state;
	}
}

export default reducer;