
export const Types = {
	SAVE_JOBS: 'SAVE_JOBS'
}

export const ActionCreators = {

	saveJobs: (data) => ({type: Types.SAVE_JOBS, payload: data } )
}

