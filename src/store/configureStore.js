import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from "redux-thunk";
import reducer from './reducers/app'

const configureStore = () => {
	return createStore(reducer, applyMiddleware(thunk));
}
export default configureStore;