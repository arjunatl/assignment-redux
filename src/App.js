import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import Header from './Components/Header/Header';
import Footer from './Components/Footer/Footer';
import JobList from './Components/JobList/JobList';

import JobDetails from './Components/JobDetails/JobDetails';
import {
  ActionCreators,
} from './store/actions/app';
import {getJobList} from './Api/JobApi';
import {connect, useDispatch} from 'react-redux';


function App() {
  
  const dispatch = useDispatch();
	getJobList().then(response => {
	  //saving the response data to store
	  dispatch(ActionCreators.saveJobs(response));  
	});

  return (
    <div className="App">
          <Router>
          <Header />
          <div className="main">
          <Switch>
            <Route exact path="/" component={JobList} />  
            <Route exact path="/jobs-detail/:id" component={JobDetails} /> 
          </Switch>
          <Footer />
          </div>

         
        </Router>




    </div>
  );
}

export default connect()(App);
