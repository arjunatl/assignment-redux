import React from "react";
import "./Footer.scss";
export default function Footer() {
  return (
    <footer className="text-center text-lg-start bg-light text-muted w-100">
        <div className="text-center p-4">
          © 2021 Copyright:
          <a className="text-reset fw-bold" href="https://mdbootstrap.com/">MDBootstrap.com</a>
        </div>
    </footer>)

}