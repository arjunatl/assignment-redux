import React from "react";
import "./JobList.scss";
import { useSelector } from 'react-redux';
import { Link } from "react-router-dom";
export default function JobList() {
	const jobs = useSelector(state => state);
	console.log('--app data---', jobs);
	return (
	
		<div className="job-container">
      <div className="container">
        <div className="row">
          {jobs.jobList.map((job,index) => (
            <div className="col-md-4" key={index}>
              <div className="job-wrapper">
                <div className="img-title-wrap">
                  <img
                    alt="sample-logo"
                    src="https://seeklogo.com/images/M/m-letter-company-logo-E2FADCFF0B-seeklogo.com.png"
                  />
                  <div className="title-loc">
                    <h5 className="mt-2">
                      <Link to={`/jobs-detail/${job.uuid}`}>{job.title}</Link>
                      <i className="fa fa-heart-o" aria-hidden="true"></i>
                    </h5>
                    <h6 className="mt-2">Technopark,Trivandrum</h6>
                  </div>
                </div>
                <p>
                  What is Lorem Ipsum Lorem Ipsum is simply dummy text of the
                  printing and typesetting industry Lorem Ipsum has been the
                  industry's standard dummy text ever since the 1500s when an
                  unknown printer took a galley of type and scrambled it to make
                  a type specimen book it has
                </p>
                <div className="tags position-relative">
                  <ul>
                    <li>Full Time</li>
                    <li>Min 1 Year</li>
                    <li>Senior Level</li>
                  </ul>
                  <span>
                    New <i> 3d</i>
                  </span>
                </div>

                <div className="row btn-wrap">
                  <div className="col">
                    <button className="apply">Apply</button>
                  </div>
                  <div className="col">
                    <button className="message col">Messages</button>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
	
	)
}