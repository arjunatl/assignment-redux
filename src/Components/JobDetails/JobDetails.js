import React, { useState, useEffect } from "react";
import "./JobDetails.scss";
import axios from "axios";
export default function JobDetails(props) {
  console.log(props.match.params.id);
  const [job, setJob] = useState();

  useEffect(() => {
    axios
      .get(`${process.env.REACT_APP_BASE_URL}jobs/${props.match.params.id}`)
      .then((response) => {
        setJob(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  }, []);
  return (      
    <div className="container detail-container">
      <div className="details_wrapper">
        <div className="img-profile">
          <img
            src="https://seeklogo.com/images/M/m-letter-company-logo-E2FADCFF0B-seeklogo.com.png"
            alt="logo"
          />
        </div>
        <div className="row">
          <div className="col pr-0">
            <div className="block-about two bg-grey-2 h-100">
              <div className="block-title">
                <h2 className="title">Job Info</h2>
              </div>

              <ul className="mt-40 info">
                <li>
                  <span>Job Name</span> : {job?.title}
                </li>
                <li>
                  <span>Job Code</span> : {job?.uuid}
                </li>
                <li>
                  <span>Phone</span> : + 123-456-789-456
                </li>
                <li>
                  <span>Email</span> : support@mutationmedia.net
                </li>
                <li>
                  <span>Skype</span> : Johanson_Doe{" "}
                </li>
                <li>
                  <span>Freelance</span> : Available
                </li>
                <li>
                  <span>Adresse</span> : 1234 Street Road, City Name, IN 567
                  890.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
