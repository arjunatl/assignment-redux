import React from "react";
import "./Header.scss";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

function Header() {

  return (
    <Navbar expand="lg" className="w-100">
      <div className="container">
        <Navbar.Brand href="#home">JobEasy</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
          <Nav className="mr-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#link">About</Nav.Link>
            <Nav.Link href="#link">
              <button className="login">Login</button>
            </Nav.Link>
            <Nav.Link href="#link">
            <button className="register">Register</button>
            </Nav.Link>
          </Nav>
          
        </Navbar.Collapse>
      </div>
    </Navbar>
  );
}
export default Header;

